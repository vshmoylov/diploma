#include "widget.h"
#include "ui_widget.h"
#include <QtConcurrent/QtConcurrent>
#include <QDebug>
#include <QMessageBox>
#include <ImageTransformations.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <QFileDialog>

using namespace cv;

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->graphicsView->setScene(&scene);

    clearImages();

    connect(ui->hMinSpinBox, SIGNAL(valueChanged(int)), ui->hMinHorizontalSlider, SLOT(setValue(int)));
    connect(ui->hMinHorizontalSlider, SIGNAL(valueChanged(int)), ui->hMinSpinBox, SLOT(setValue(int)));
    connect(ui->hMaxSpinBox, SIGNAL(valueChanged(int)), ui->hMaxHorizontalSlider, SLOT(setValue(int)));
    connect(ui->hMaxHorizontalSlider, SIGNAL(valueChanged(int)), ui->hMaxSpinBox, SLOT(setValue(int)));

    connect(ui->sMinSpinBox, SIGNAL(valueChanged(int)), ui->sMinHorizontalSlider, SLOT(setValue(int)));
    connect(ui->sMinHorizontalSlider, SIGNAL(valueChanged(int)), ui->sMinSpinBox, SLOT(setValue(int)));
    connect(ui->sMaxSpinBox, SIGNAL(valueChanged(int)), ui->sMaxHorizontalSlider, SLOT(setValue(int)));
    connect(ui->sMaxHorizontalSlider, SIGNAL(valueChanged(int)), ui->sMaxSpinBox, SLOT(setValue(int)));

    connect(ui->vMinSpinBox, SIGNAL(valueChanged(int)), ui->vMinHorizontalSlider, SLOT(setValue(int)));
    connect(ui->vMinHorizontalSlider, SIGNAL(valueChanged(int)), ui->vMinSpinBox, SLOT(setValue(int)));
    connect(ui->vMaxSpinBox, SIGNAL(valueChanged(int)), ui->vMaxHorizontalSlider, SLOT(setValue(int)));
    connect(ui->vMaxHorizontalSlider, SIGNAL(valueChanged(int)), ui->vMaxSpinBox, SLOT(setValue(int)));

    ui->hMinSpinBox->setValue(0);
    ui->hMaxSpinBox->setValue(180);
    ui->sMinSpinBox->setValue(0);
    ui->sMaxSpinBox->setValue(80);
    ui->vMinSpinBox->setValue(35);
    ui->vMaxSpinBox->setValue(256);
    ui->pushButton->click();
    ui->retinexPushButton->setVisible(false);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_clicked()
{
    QFile imagesList("../test1/ImagesList.txt");
    if (!imagesList.open(QIODevice::ReadOnly)) return;
    QString fname = imagesList.readLine().trimmed();
    imagesList.close();

    this->setEnabled(false);
    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
    QtConcurrent::run(this, &Widget::loadImage, fname);
}

void Widget::on_pushButton_2_clicked()
{
    int Hmin = ui->hMinSpinBox->value();
    int Hmax = ui->hMaxSpinBox->value();
    int Smin = ui->sMinSpinBox->value();
    int Smax = ui->sMaxSpinBox->value();
    int Vmin = ui->vMinSpinBox->value();
    int Vmax = ui->vMaxSpinBox->value();
    Mat hsv_and;
    Mat h_range, s_range, v_range;
    inRange(HSV[0], Scalar(Hmin), Scalar(Hmax), h_range);
    inRange(HSV[1], Scalar(Smin), Scalar(Smax), s_range);
    inRange(HSV[2], Scalar(Vmin), Scalar(Vmax), v_range);
    bitwise_and(h_range, s_range, hsv_and);
    bitwise_and(hsv_and, v_range, hsv_and);
    //    currentImage = hsv_and;
    //    bitwise_and(originalImage, hsv_and, currentImage);
    //    originalImage.copyTo(currentImage,hsv_and);
    double alpha = 0.8;
    double beta = 1.0-alpha;
    Mat temp,temp2=originalImage;
    mask = hsv_and;
    cvtColor(hsv_and,temp,CV_GRAY2BGR);
    currentImage=Mat();
    addWeighted(temp2, alpha, temp, beta, 0.0, currentImage);
    updateImage();
}

void Widget::imageLoaded()
{
    currentImage = originalImage;
    updateImage();
    ui->plainTextEdit->appendPlainText(QString::fromUtf8("Информация об изображении:\nШирина: %1\nВысота: %2").arg(originalImage.cols).arg(originalImage.rows));
    this->setEnabled(true);
    QApplication::restoreOverrideCursor();
}

void Widget::updateImage()
{
    QImage qimg = cvMatToQImage(currentImage);

    scene.clear();
    scene.addPixmap(QPixmap::fromImage(qimg).scaled(ui->graphicsView->size().width()-2,
                                                    ui->graphicsView->size().height()-2,
                                                    Qt::KeepAspectRatio,Qt::SmoothTransformation));
}

void Widget::resizeEvent(QResizeEvent *evt)
{
    updateImage();
    evt->accept();
}

void Widget::clearImages()
{
    originalImage = Mat();
    currentImage = Mat();
    RGB = QVector<Mat>(3);
    HSV = QVector<Mat>(3);
    channels.append(&RGB);
    channels.append(&HSV);
}

void Widget::loadImage(QString filename)
{
    filename.replace("\\", "/");
    //    subdir.replace("\\", "/");
    QFileInfo fileInfo(filename);

    if (!fileInfo.exists())
        return;
    clearImages();
    originalImage = imread(filename.toStdString());

    if (originalImage.data == NULL){
        QMessageBox::critical(this,"Image Load","Cannot load image: "+filename);
        return;
    }

    Mat tmp;
    cvtColor(originalImage, tmp, CV_BGR2RGB);
    split(tmp, RGB.data());

    cvtColor(originalImage, tmp, CV_BGR2HSV);
    split(tmp, HSV.data());

    QMetaObject::invokeMethod(this, "imageLoaded");
}

void Widget::on_pushButton_3_clicked()
{
    QString fname = QFileDialog::getSaveFileName(this);
    if (fname.isEmpty()) return;
    imwrite(fname.toStdString(),currentImage);
}

void Widget::on_pushButton_4_clicked()
{
    Mat temp;
    temp = copyMask(RGB[0], mask);
    currentImage = Mat();
    currentImage = temp;
//    equalizeHist(temp, currentImage);
    updateImage();
}

void Widget::on_pushButton_5_clicked()
{
    currentImage = quantize(currentImage, ui->spinBox->value());
    updateImage();
}

void Widget::on_pushButton_6_clicked()
{
    int x = originalImage.cols/2;
    int y = originalImage.rows/2;
    int r = originalImage.cols/2-originalImage.cols/100;
    int newSize = r*2;
    int cropX = (originalImage.cols-newSize)/2;
    int cropY = 0;
    int cropWidth = newSize;
    int cropHeight = originalImage.rows;
    currentImage = /*letterboxImage(*/cropImage(cutCircle(currentImage, x, y, r),cropX, cropY, cropWidth, cropHeight)/*, newSize, newSize)*/;
    int count = 0;
    int allcount = 0;

    Mat imageMask(originalImage.rows, originalImage.cols, CV_8UC1);
    circle(imageMask, Point(x,y), r, 255, -1);
    for (int i=0; i<imageMask.cols; i++){
        for (int j=0; j<imageMask.rows; j++){
            if (imageMask.at<uchar>(j,i)==255)
                allcount++;
        }
    }

    for (int i=0; i<currentImage.cols; i++){
        for (int j=0; j<currentImage.rows; j++){
            if (currentImage.at<uchar>(j,i)!=0)
                count++;
        }
    }
    ui->plainTextEdit->appendPlainText(QString("All: %1\nCount: %2\nPercentage: %3%").arg(allcount).arg(count).arg((double)count/allcount*100.0));
    updateImage();
}


void Widget::on_retinexPushButton_clicked()
{

}
