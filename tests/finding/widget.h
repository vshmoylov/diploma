#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QGraphicsScene>

#include <opencv2/core/core.hpp>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();
    void imageLoaded();
    void updateImage();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();


    void on_retinexPushButton_clicked();

protected:
    void resizeEvent(QResizeEvent *evt);

private:
    Ui::Widget *ui;
    QGraphicsScene scene;
    cv::Mat originalImage;
    cv::Mat currentImage;
    QVector<cv::Mat> RGB;
    QVector<cv::Mat> HSV;
    cv::Mat mask;
//    QVector<cv::Mat> HSV;
    QVector< QVector<cv::Mat> *> channels;
    void clearImages();
    void loadImage(QString filename);
};

#endif // WIDGET_H
