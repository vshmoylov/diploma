#include "widget.h"
#include "ui_widget.h"
#include <QtConcurrent/QtConcurrent>
#include <QDebug>
#include <QMessageBox>
#include <ImageTransformations.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <QFileDialog>

using namespace cv;

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->graphicsView->setScene(&scene);

    clearImages();

//    connect(ui->xSpinBox,SIGNAL(valueChanged(int)), this, SLOT(setCircle()));
//    connect(ui->ySpinBox,SIGNAL(valueChanged(int)), this, SLOT(setCircle()));
//    connect(ui->rSpinBox,SIGNAL(valueChanged(int)), this, SLOT(setCircle()));

    ui->pushButton->click();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_clicked()
{
    QFile imagesList("../test1/ImagesList.txt");
    if (!imagesList.open(QIODevice::ReadOnly)) return;
    QString fname = imagesList.readLine().trimmed();
    imagesList.close();

    this->setEnabled(false);
    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
    QtConcurrent::run(this, &Widget::loadImage, fname);
}

void Widget::on_pushButton_2_clicked()
{
    setCircle();
    updateImage();
}

void Widget::imageLoaded()
{
    currentImage = originalImage;
    updateImage();
    ui->plainTextEdit->appendPlainText(QString::fromUtf8("Информация об изображении:\nШирина: %1\nВысота: %2").arg(originalImage.cols).arg(originalImage.rows));
    this->setEnabled(true);
    QApplication::restoreOverrideCursor();
    ui->xSpinBox->setValue(originalImage.cols/2);
    ui->ySpinBox->setValue(originalImage.rows/2);
    ui->rSpinBox->setValue(originalImage.cols/2-originalImage.cols/100);
}

void Widget::updateImage()
{
    QImage qimg = cvMatToQImage(currentImage);

    scene.clear();
    scene.addPixmap(QPixmap::fromImage(qimg).scaled(ui->graphicsView->size().width()-2,
                                                    ui->graphicsView->size().height()-2,
                                                    Qt::KeepAspectRatio,Qt::SmoothTransformation));
}

void Widget::setCircle()
{
    int x, y, r;
    x=ui->xSpinBox->value();
    y=ui->ySpinBox->value();
    r=ui->rSpinBox->value();
    int newSize = r*2;
//    //int sz=r*2+10;

//    //Mat img(currentImage.size, currentImage.type());
//    Mat imageMask(originalImage.rows, originalImage.cols, CV_8UC1);
//    circle(imageMask, Point(x,y), r, 255, -1);


////    imageMask.copyTo(currentImage);
//    currentImage = Mat();
//    originalImage.copyTo(currentImage, imageMask);
//    Mat temp;
//    int newWidth = r*2;
//    newWidth=newWidth!=0?newWidth:originalImage.cols;
//    int cropPad = (originalImage.cols-newWidth)/2.0;
//    Mat newImg;
//    Mat(currentImage, Rect(cropPad, 0, newWidth, originalImage.rows)).copyTo(newImg);
////    currentImage = Mat();
//    newImg.copyTo(currentImage);
////    int newWidth = r*2+2;
//    int newHeight = newWidth;
//    int padVertical = abs(newHeight-currentImage.rows)/2;
//    int padVertical2 = abs(newHeight-currentImage.rows)-padVertical;
//    currentImage=Mat();
////    newImg.copyTo(currentImage);
//    copyMakeBorder(newImg, currentImage, padVertical, padVertical2, 0, 0, BORDER_CONSTANT, Scalar(0,0,0));



    int cropX = (originalImage.cols-newSize)/2;
    int cropY = 0;
    int cropWidth = newSize;
    int cropHeight = originalImage.rows;
    currentImage = letterboxImage(cropImage(cutCircle(currentImage, x, y, r),cropX, cropY, cropWidth, cropHeight), newSize, newSize);

    updateImage();
}

void Widget::resizeEvent(QResizeEvent *evt)
{
    updateImage();
    evt->accept();
}

void Widget::clearImages()
{
    originalImage = Mat();
    currentImage = Mat();
    RGB = QVector<Mat>(3);
    HSV = QVector<Mat>(3);
    channels.append(&RGB);
    channels.append(&HSV);
}

void Widget::loadImage(QString filename)
{
    filename.replace("\\", "/");
//    subdir.replace("\\", "/");
    QFileInfo fileInfo(filename);

    if (!fileInfo.exists())
        return;
    clearImages();
    originalImage = imread(filename.toStdString());

    if (originalImage.data == NULL){
        QMessageBox::critical(this,"Image Load","Cannot load image: "+filename);
        return;
    }

    Mat tmp;
    cvtColor(originalImage, tmp, CV_BGR2RGB);
    split(tmp, RGB.data());

    cvtColor(originalImage, tmp, CV_BGR2HSV);
    split(tmp, HSV.data());

    currentImage = originalImage;

    QMetaObject::invokeMethod(this, "imageLoaded");
}

void Widget::on_pushButton_3_clicked()
{
    QString fname = QFileDialog::getSaveFileName(this);
    if (fname.isEmpty()) return;
    imwrite(fname.toStdString(),currentImage);
}

void rotate(cv::Mat& src, double angle, cv::Mat& dst)
{
    int len = std::max(src.cols, src.rows);
    cv::Point2f pt(len/2., len/2.);
    cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);

    cv::warpAffine(src, dst, r, cv::Size(len, len));
}

void Widget::on_pushButton_4_clicked()
{
    currentImage=unwrapHorizontally(currentImage);

    updateImage();
}
