#ifndef IMAGETRANSFORMATIONS_H
#define IMAGETRANSFORMATIONS_H

//operation time macro
#define GET_TIME(X, TIME) {\
    QTime GET_TIME_start = QTime::currentTime(); \
    X; \
    QTime GET_TIME_end = QTime::currentTime(); \
    TIME = GET_TIME_start.msecsTo(GET_TIME_end); \
}

class QImage;
namespace cv { class Mat; }

QImage cvMatToQImage( const cv::Mat &inMat );
cv::Mat adjustContrast(const cv::Mat &src, int thresh);
cv::Mat cutCircle(const cv::Mat &src, int x, int y, int radius);
cv::Mat cropImage(const cv::Mat &src, int x, int y, int width , int height);
cv::Mat letterboxImage(const cv::Mat &src, int width, int height);
cv::Mat unwrapHorizontally(const cv::Mat &src, bool linearInterpolate=false);

#endif // IMAGETRANSFORMATIONS_H
