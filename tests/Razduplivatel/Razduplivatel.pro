#-------------------------------------------------
#
# Project created by QtCreator 2014-05-10T16:05:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Razduplivatel
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    ImageTransformations.cpp

HEADERS  += widget.h \
    ImageTransformations.h

FORMS    += widget.ui


OPENCV_PATH = C:/opencv
INCLUDEPATH += $${OPENCV_PATH}"/include"

OPENCV_LIBPATH = "..\libs"

win32-g++ {
    CONFIG(debug, debug|release) {
        OPENCV_LIBPATH = $${OPENCV_PATH}/mingw-qt5/debug
    } else {
        OPENCV_LIBPATH = $${OPENCV_PATH}/mingw-qt5/release
    }
}

win32-msvc2010 {
    CONFIG(debug, debug|release) {
        OPENCV_LIBPATH = $${OPENCV_PATH}/msvc2010/debug
    } else {
        OPENCV_LIBPATH = $${OPENCV_PATH}/msvc2010/release
    }
}

LIBS += -L$${OPENCV_LIBPATH}
DEPENDPATH += $${OPENCV_LIBPATH}

Release {
LIBS += -lopencv_calib3d248 \
        -lopencv_contrib248 \
        -lopencv_core248 \
        -lopencv_features2d248 \
        -lopencv_flann248 \
        -lopencv_gpu248 \
        -lopencv_highgui248 \
        -lopencv_imgproc248 \
        -lopencv_legacy248 \
        -lopencv_ml248 \
        -lopencv_nonfree248 \
        -lopencv_objdetect248 \
        -lopencv_ocl248 \
        -lopencv_photo248 \
        -lopencv_stitching248 \
        -lopencv_superres248 \
        -lopencv_ts248 \
        -lopencv_video248 \
        -lopencv_videostab248
}

Debug {
LIBS += -lopencv_calib3d248d \
        -lopencv_contrib248d \
        -lopencv_core248d \
        -lopencv_features2d248d \
        -lopencv_flann248d \
        -lopencv_gpu248d \
        -lopencv_highgui248d \
        -lopencv_imgproc248d \
        -lopencv_legacy248d \
        -lopencv_ml248d \
        -lopencv_nonfree248d \
        -lopencv_objdetect248d \
        -lopencv_ocl248d \
        -lopencv_photo248d \
        -lopencv_stitching248d \
        -lopencv_superres248d \
        -lopencv_ts248d \
        -lopencv_video248d \
        -lopencv_videostab248d
}
