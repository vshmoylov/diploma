#include <imageTransformations.h>
#include <QImage>
#include <QDebug>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;

QImage cvMatToQImage( const cv::Mat &inMat ) {
    switch ( inMat.type() )
    {
    // 8-bit, 4 channel
    case CV_8UC4:
    {
        QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB32 );

        return image;
    }

    // 8-bit, 3 channel
    case CV_8UC3:
    {
        QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB888 );

        return image.rgbSwapped();
    }

    // 8-bit, 1 channel
    case CV_8UC1:
    {
        static QVector<QRgb>  sColorTable;

        // only create our color table once
        if ( sColorTable.isEmpty() )
        {
            for ( int i = 0; i < 256; ++i )
                sColorTable.push_back( qRgb( i, i, i ) );
        }

        QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_Indexed8 );

        image.setColorTable( sColorTable );

        return image;
    }

    default:
        qWarning() << "cvMatToQImage() - cv::Mat image type not handled in switch:" << inMat.type();
        break;
    }

    return QImage();
}

cv::Mat adjustContrast(const cv::Mat &src, int thresh){
    cv::Mat result(src);

    double contrastLevel = pow((100.0 + thresh) / 100.0, 2);

    double blue = 0;
    double green = 0;
    double red = 0;

    int ch = result.channels();
    int ind;

    for (int i=0; i<result.rows; i++){
        for (int j=0; j<result.cols; j++){
            ind = i*result.cols*ch+j*ch;
            blue = ((((result.data[ind] / 255.0) - 0.5) *
                    contrastLevel) + 0.5) * 255.0;
            green = ((((result.data[ind+1] / 255.0) - 0.5) *
                    contrastLevel) + 0.5) * 255.0;
            red = ((((result.data[ind+2] / 255.0) - 0.5) *
                    contrastLevel) + 0.5) * 255.0;

            if  (blue > 255) { blue = 255; }
            else if  (blue < 0) { blue = 0; }

            if (green > 255) { green = 255; }
            else if (green < 0) { green = 0; }

            if (red > 255) { red = 255; }
            else if (red < 0) { red = 0; }

            result.data[ind]=blue;
            result.data[ind+1]=green;
            result.data[ind+2]=red;
        }
    }

    return result;
}




cv::Mat cutCircle(const cv::Mat &src, int x, int y, int radius)
{
    Mat imageMask(src.rows, src.cols, CV_8UC1);
    circle(imageMask, Point(x,y), radius, 255, -1);

    Mat result;
    src.copyTo(result, imageMask);
    return result;
}


cv::Mat cropImage(const cv::Mat &src, int x, int y, int width, int height)
{
    Mat result;
    Mat(src, Rect(x, y, width, height)).copyTo(result);
    return result;
}


cv::Mat letterboxImage(const cv::Mat &src, int width, int height)
{
    int currentWidth = src.cols;
    int currentHeight = src.rows;
    Mat result;
    if (currentHeight>height || currentWidth>width) {src.copyTo(result); return result;}
    int paddingTop = (height-currentHeight)/2;
    int paddingBottom = height-currentHeight-paddingTop;
    int paddingLeft = (width - currentWidth)/2;
    int paddingRight = width - currentWidth - paddingLeft;
    copyMakeBorder(src, result, paddingTop, paddingBottom, paddingLeft, paddingRight,BORDER_CONSTANT, Scalar(0,0,0));
    return result;
}


cv::Mat unwrapHorizontally(const cv::Mat &src, bool linearInterpolate)
{
    int origSize = src.rows;
    int l = origSize/2;
    Mat result(l, 4*l, src.type());

    int i, j;
    int x, y;
    double radius, theta;

    // for use in neighbouring indices in Cartesian coordinates
    int iFloorX, iCeilingX, iFloorY, iCeilingY;
    // calculated indices in Cartesian coordinates with trailing decimals
    double fTrueX, fTrueY;
    // for interpolation
    double fDeltaX, fDeltaY;
    // pixel colours
    Scalar clrTopLeft, clrTopRight, clrBottomLeft, clrBottomRight;
    // interpolated "top" pixels
    double fTopRed, fTopGreen, fTopBlue;
    // interpolated "bottom" pixels
    double fBottomRed, fBottomGreen, fBottomBlue;
    // final interpolated colour components
    int iRed, iGreen, iBlue;

    int resultHeight = l;
    int resultWidth  = 4*l;
    for (i = 0; i < resultHeight; ++i)
    {
        for (j = 0; j < resultWidth; ++j)
        {
            radius = (double)(l - i);
            // theta = 2.0 * Math.PI * (double)(4.0 * l - j) / (double)(4.0 * l);
            theta = 2.0 * M_PI * (double)(-j) / (double)(4.0 * l);

            fTrueX = radius * cos(theta);
            fTrueY = radius * sin(theta);

            // "normal" mode
            x = (int)(roundl(fTrueX)) + l;
            y = l - (int)(roundl(fTrueY));
            // check bounds
            if (x >= 0 && x < (2 * l) && y >= 0 && y < (2 * l))
            {
                result.at<Vec3b>((resultHeight-1)-i,j)=src.at<Vec3b>(y,x);
//                bmDestination.SetPixel(j, i, bm.GetPixel(x, y));
            }

          /*  // bilinear mode
            fTrueX = fTrueX + (double)l;
            fTrueY = (double)l - fTrueY;

            iFloorX = (int)(Math.Floor(fTrueX));
            iFloorY = (int)(Math.Floor(fTrueY));
            iCeilingX = (int)(Math.Ceiling(fTrueX));
            iCeilingY = (int)(Math.Ceiling(fTrueY));

            // check bounds
            if (iFloorX < 0 || iCeilingX < 0 ||
                iFloorX >= (2 * l) || iCeilingX >= (2 * l) ||
                iFloorY < 0 || iCeilingY < 0 ||
                iFloorY >= (2 * l) || iCeilingY >= (2 * l)) continue;

            fDeltaX = fTrueX - (double)iFloorX;
            fDeltaY = fTrueY - (double)iFloorY;

            clrTopLeft = bm.GetPixel(iFloorX, iFloorY);
            clrTopRight = bm.GetPixel(iCeilingX, iFloorY);
            clrBottomLeft = bm.GetPixel(iFloorX, iCeilingY);
            clrBottomRight = bm.GetPixel(iCeilingX, iCeilingY);

            // linearly interpolate horizontally between top neighbours
            fTopRed = (1 - fDeltaX) * clrTopLeft.R + fDeltaX * clrTopRight.R;
            fTopGreen = (1 - fDeltaX) * clrTopLeft.G + fDeltaX * clrTopRight.G;
            fTopBlue = (1 - fDeltaX) * clrTopLeft.B + fDeltaX * clrTopRight.B;

            // linearly interpolate horizontally between bottom neighbours
            fBottomRed = (1 - fDeltaX) * clrBottomLeft.R + fDeltaX * clrBottomRight.R;
            fBottomGreen = (1 - fDeltaX) * clrBottomLeft.G + fDeltaX * clrBottomRight.G;
            fBottomBlue = (1 - fDeltaX) * clrBottomLeft.B + fDeltaX * clrBottomRight.B;

            // linearly interpolate vertically between top and bottom interpolated results
            iRed = (int)(Math.Round((1 - fDeltaY) * fTopRed + fDeltaY * fBottomRed));
            iGreen = (int)(Math.Round((1 - fDeltaY) * fTopGreen + fDeltaY * fBottomGreen));
            iBlue = (int)(Math.Round((1 - fDeltaY) * fTopBlue + fDeltaY * fBottomBlue));

            // make sure colour values are valid
            if (iRed < 0) iRed = 0;
            if (iRed > 255) iRed = 255;
            if (iGreen < 0) iGreen = 0;
            if (iGreen > 255) iGreen = 255;
            if (iBlue < 0) iBlue = 0;
            if (iBlue > 255) iBlue = 255;

            bmBilinear.SetPixel(j, i, Color.FromArgb(iRed, iGreen, iBlue));*/
        }
    }
    return result;
}
