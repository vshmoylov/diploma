
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdlib.h>
#include <stdio.h>

int tresholdMax = 250;
int tresholdVal = 50;
int radius = 7;
int radiusPos = 3;
int minus = 1;
int RGBmax = 256;

IplImage *src=0, *dst=0, *dst2=0, *dst3=0;


void maxValueTrackbar(int pos){
    tresholdMax = pos;
    cvThreshold(src, dst, tresholdVal, tresholdMax, CV_THRESH_BINARY);
    cvAdaptiveThreshold(src, dst2, tresholdMax, CV_ADAPTIVE_THRESH_MEAN_C,      CV_THRESH_BINARY, radius, minus);
    cvAdaptiveThreshold(src, dst3, tresholdMax, CV_ADAPTIVE_THRESH_GAUSSIAN_C,  CV_THRESH_BINARY, radius, minus);
}

void threshValTrackbar(int pos){
    tresholdVal=pos;
    cvThreshold(src, dst, tresholdVal, tresholdMax, CV_THRESH_BINARY);
}

void radiusTrackbar(int pos){

    radiusPos = pos;
    radius=radiusPos*2+1;
    cvAdaptiveThreshold(src, dst2, tresholdMax, CV_ADAPTIVE_THRESH_MEAN_C,      CV_THRESH_BINARY, radius, minus);
    cvAdaptiveThreshold(src, dst3, tresholdMax, CV_ADAPTIVE_THRESH_GAUSSIAN_C,  CV_THRESH_BINARY, radius, minus);
}

void minusTrackbar(int pos){
    minus=pos;
    cvAdaptiveThreshold(src, dst2, tresholdMax, CV_ADAPTIVE_THRESH_MEAN_C,      CV_THRESH_BINARY, radius, minus);
    cvAdaptiveThreshold(src, dst3, tresholdMax, CV_ADAPTIVE_THRESH_GAUSSIAN_C,  CV_THRESH_BINARY, radius, minus);
}

int main(int argc, char* argv[])
{

        // имя картинки задаётся первым параметром
        char* filename = (char*)(argc >= 2 ? argv[1] : "Image0.jpg");
        // получаем картинку
        src = cvLoadImage(filename, 0);

        printf("[i] image: %s\n", filename);
        assert( src != 0 );

        // покажем изображение
        cvNamedWindow( "original", 1 );
        cvShowImage( "original", src );

        dst = cvCreateImage( cvSize(src->width, src->height), IPL_DEPTH_8U, 1);
        dst2 = cvCreateImage( cvSize(src->width, src->height), IPL_DEPTH_8U, 1);
        dst3 = cvCreateImage( cvSize(src->width, src->height), IPL_DEPTH_8U, 1);

        cvThreshold(src, dst, 50, 250, CV_THRESH_BINARY);
        cvAdaptiveThreshold(src, dst2, 250, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 7, 1);
        cvAdaptiveThreshold(src, dst3, 250, CV_ADAPTIVE_THRESH_GAUSSIAN_C, CV_THRESH_BINARY, 7, 1);
        cvCreateTrackbar("MAX",     "original", &tresholdMax, RGBmax, maxValueTrackbar);
        cvCreateTrackbar("VAL",     "original", &tresholdVal, RGBmax, threshValTrackbar);
        cvCreateTrackbar("radius",  "original", &radiusPos,      10, radiusTrackbar);
        cvCreateTrackbar("minus",   "original", &minus,       50, minusTrackbar);

        // показываем результаты
        while (true){
            cvNamedWindow( "cvThreshold", 1 );
            cvShowImage( "cvThreshold", dst);
            cvNamedWindow( "cvAdaptiveThreshold", 1 );
            cvShowImage( "cvAdaptiveThreshold", dst2);
            cvNamedWindow( "cvAdaptiveThreshold2", 1 );
            cvShowImage( "cvAdaptiveThreshold2", dst3);
            char c = cvWaitKey(33);
            if (c == 27) { // если нажата ESC - выходим
                break;
            }
        }



        // освобождаем ресурсы
        cvReleaseImage(&src);
        cvReleaseImage(&dst);
        cvReleaseImage(&dst2);
        cvReleaseImage(&dst3);
        // удаляем окна
        cvDestroyAllWindows();
        return 0;
}
