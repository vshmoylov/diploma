#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QGraphicsScene>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

namespace Ui {
class Widget;
}

using namespace cv;

enum ColorModel {
    ColorModel_RGB, ColorModel_HLS, ColorModel_HSV
};

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_pushButton_clicked();

    void on_comboBox_currentIndexChanged(int index);
    void imageLoaded();
    void updateImage();

    void on_pushButton_2_clicked();

protected:
    void resizeEvent(QResizeEvent *evt);

private:
    Ui::Widget *ui;
    QGraphicsScene scene;
    Mat originalImage;
    Mat currentImage;
    QVector<Mat> RGB;
    QVector<Mat> HLS;
    QVector<Mat> HSV;
    QVector< QVector<Mat> *> channels;
    ColorModel currentColorModel;
    void clearImages();
    void loadImage(QString filename);
    Mat quantizeImage(const Mat& img, int levels);
    void loadImage(QString filename, bool saveChannels, QString subdir);
    void log(QString function, QString message);
    void clearLog();
    void log(QString message);

};

#endif // WIDGET_H
