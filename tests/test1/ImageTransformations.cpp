#include <imageTransformations.h>
#include <QImage>
#include <QDebug>
#include <opencv2/core/core.hpp>

QImage cvMatToQImage( const cv::Mat &inMat ) {
    switch ( inMat.type() )
    {
    // 8-bit, 4 channel
    case CV_8UC4:
    {
        QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB32 );

        return image;
    }

    // 8-bit, 3 channel
    case CV_8UC3:
    {
        QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB888 );

        return image.rgbSwapped();
    }

    // 8-bit, 1 channel
    case CV_8UC1:
    {
        static QVector<QRgb>  sColorTable;

        // only create our color table once
        if ( sColorTable.isEmpty() )
        {
            for ( int i = 0; i < 256; ++i )
                sColorTable.push_back( qRgb( i, i, i ) );
        }

        QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_Indexed8 );

        image.setColorTable( sColorTable );

        return image;
    }

    default:
        qWarning() << "cvMatToQImage() - cv::Mat image type not handled in switch:" << inMat.type();
        break;
    }

    return QImage();
}

cv::Mat adjustContrast(const cv::Mat &src, int thresh){
    cv::Mat result(src);

    double contrastLevel = pow((100.0 + thresh) / 100.0, 2);

    double blue = 0;
    double green = 0;
    double red = 0;

    int ch = result.channels();
    int ind;

    for (int i=0; i<result.rows; i++){
        for (int j=0; j<result.cols; j++){
            ind = i*result.cols*ch+j*ch;
            blue = ((((result.data[ind] / 255.0) - 0.5) *
                    contrastLevel) + 0.5) * 255.0;
            green = ((((result.data[ind+1] / 255.0) - 0.5) *
                    contrastLevel) + 0.5) * 255.0;
            red = ((((result.data[ind+2] / 255.0) - 0.5) *
                    contrastLevel) + 0.5) * 255.0;

            if  (blue > 255) { blue = 255; }
            else if  (blue < 0) { blue = 0; }

            if (green > 255) { green = 255; }
            else if (green < 0) { green = 0; }

            if (red > 255) { red = 255; }
            else if (red < 0) { red = 0; }

            result.data[ind]=blue;
            result.data[ind+1]=green;
            result.data[ind+2]=red;
        }
    }

    return result;
}
