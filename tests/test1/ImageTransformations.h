#ifndef IMAGETRANSFORMATIONS_H
#define IMAGETRANSFORMATIONS_H

//operation time macro
#define GET_TIME(X, TIME) {\
    QTime GET_TIME_start = QTime::currentTime(); \
    X; \
    QTime GET_TIME_end = QTime::currentTime(); \
    TIME = GET_TIME_start.msecsTo(GET_TIME_end); \
}

class QImage;
namespace cv { class Mat; }

QImage cvMatToQImage( const cv::Mat &inMat );
cv::Mat adjustContrast(const cv::Mat &src, int thresh);

#endif // IMAGETRANSFORMATIONS_H
