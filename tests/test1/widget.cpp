#include "widget.h"
#include "ui_widget.h"
#include <QtConcurrent/QtConcurrent>

#include <QDebug>
#include <QMessageBox>
#include <QtCore>
#include <QResizeEvent>
#include <imageTransformations.h>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->graphicsView->setScene(&scene);


    QAbstractItemModel *model = ui->comboBox->model();
    for (int i=0; i<3; i++){
        model->setData(model->index(i*4,0), QVariant(0), Qt::UserRole-1);
    }

    clearImages();
    ui->progressBar->setVisible(false);
    connect (ui->quantizeGroupBox, SIGNAL(toggled(bool)), this, SLOT(updateImage()));
    connect (ui->spinBox, SIGNAL(valueChanged(int)), this, SLOT(updateImage()));

}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_clicked()
{
    QFile imagesList("../test1/ImagesList.txt");
    if (!imagesList.open(QIODevice::ReadOnly)) return;
    QString fname = imagesList.readLine().trimmed();
    imagesList.close();

    ui->pushButton->setEnabled(false);

    QtConcurrent::run(this, &Widget::loadImage, fname);
}

void Widget::clearImages()
{
    originalImage = Mat();
    currentImage = Mat();
    RGB = QVector<Mat>(3);
    HLS = QVector<Mat>(3);
    HSV = QVector<Mat>(3);
    channels.append(&RGB);
    channels.append(&HLS);
    channels.append(&HSV);
}

void Widget::loadImage(QString filename)
{
//    int time;
//    GET_TIME(
    loadImage(filename, false, "ch");//, time);
//    ui->logLabel->setText(QString("Time taken: %1").arg(time));
}

Mat Widget::quantizeImage(const Mat &img, int levels)
{
    //    if (levels<2 || img.type()!=CV_8UC1) return Mat();
    Mat result(img.rows, img.cols, CV_8UC1, 256/levels);
    result = (img/result).mul(result);
    //    for (int i=0; i<img.rows; i++){
    //        for (int j=0; j<img.cols; i++){
    //            result.data[i*img.rows+j]=(result.data[i*img.rows+j]/levels)*levels;
    //        }
    //    }
    return result;
}



void Widget::loadImage(QString filename, bool saveChannels, QString subdir)
{
    filename.replace("\\", "/");
    subdir.replace("\\", "/");
    QFileInfo fileInfo(filename);

    if (!fileInfo.exists())
        return;
    clearImages();
    log("loadImage", "loading source image");
    originalImage = imread(filename.toStdString());
    //    originalImage.convertTo(originalImage,-1, ui->lightnessDoubleSpinBox->value(),ui->contrastDoubleSpinBox->value());
    if (ui->constrastGroupBox->isChecked()) originalImage = adjustContrast(originalImage,ui->contrastSpinBox->value());

    if (originalImage.data == NULL){
        QMessageBox::critical(this,"Image Load","Cannon load image: "+filename);
        return;
    }

    Mat tmp;
    log("loadImage", "converting to RGB");
    cvtColor(originalImage, tmp, CV_BGR2RGB);
    split(tmp, RGB.data());

    log("loadImage", "converting to HSV");
    cvtColor(originalImage, tmp, CV_BGR2HSV);
    split(tmp, HSV.data());

    log("loadImage", "converting to HLS");
    cvtColor(originalImage, tmp, CV_BGR2HLS);
    split(tmp, HLS.data());

    if (saveChannels){
        if (!subdir.endsWith('/')) subdir.append('/');
        QString newFileName = fileInfo.absolutePath()+subdir+fileInfo.baseName()+"_%1_%2."+fileInfo.completeSuffix();

        log("loadImage", "saving RGB channels");
        imwrite(newFileName.arg("RGB","R").toStdString(), RGB[0]);
        imwrite(newFileName.arg("RGB","G").toStdString(), RGB[1]);
        imwrite(newFileName.arg("RGB","B").toStdString(), RGB[2]);

        log("loadImage", "saving HSV channels");
        imwrite(newFileName.arg("HSV","H").toStdString(), HSV[0]);
        imwrite(newFileName.arg("HSV","S").toStdString(), HSV[1]);
        imwrite(newFileName.arg("HSV","V").toStdString(), HSV[2]);

        log("loadImage", "saving HLS channels");
        imwrite(newFileName.arg("HLS","H").toStdString(), HLS[0]);
        imwrite(newFileName.arg("HLS","L").toStdString(), HLS[1]);
        imwrite(newFileName.arg("HLS","S").toStdString(), HLS[2]);
    }
    log("loading done");
    QMetaObject::invokeMethod(this, "imageLoaded");
}

void Widget::log(QString function, QString message)
{
    log("function " + function + "(): " + message);
}

void Widget::clearLog()
{
    ui->logLabel->clear();
}

void Widget::log(QString message)
{
    QString logString = QTime::currentTime().toString()+"  "+message;
    qDebug() << logString;
    ui->logLabel->setText(message);
}

void Widget::on_comboBox_currentIndexChanged(int index)
{
    currentColorModel = (ColorModel)((index)/4);
    currentImage=((channels[currentColorModel])->operator [](index-currentColorModel*4-1));
    QImage qimg = cvMatToQImage(currentImage);

    scene.clear();
    scene.addPixmap(QPixmap::fromImage(qimg).scaled(ui->graphicsView->size().width()-2,
                                                    ui->graphicsView->size().height()-2,
                                                    Qt::KeepAspectRatio,Qt::SmoothTransformation));
}

void Widget::imageLoaded()
{
    ui->pushButton->setEnabled(true);
    updateImage();
}

void Widget::updateImage()
{
    int index = ui->comboBox->currentIndex();
    currentColorModel = (ColorModel)((index)/4);
    currentImage=((channels[currentColorModel])->operator [](index-currentColorModel*4-1));
    if (ui->quantizeGroupBox->isChecked()){
        currentImage = quantizeImage(currentImage, ui->spinBox->value());
    }
    QImage qimg = cvMatToQImage(currentImage);

    scene.clear();
    scene.addPixmap(QPixmap::fromImage(qimg).scaled(ui->graphicsView->size().width()-2,
                                                    ui->graphicsView->size().height()-2,
                                                    Qt::KeepAspectRatio,Qt::SmoothTransformation));
}

void Widget::resizeEvent(QResizeEvent *evt)
{
    updateImage();
    evt->accept();
}

void Widget::on_pushButton_2_clicked()
{
    int histSize = 256;
    float range[] = { 0, 256 } ;
    const float* histRange = { range };

    bool uniform = true; bool accumulate = false;

    Mat b_hist, g_hist, r_hist;

    /// Compute the histograms:
    calcHist( &RGB[2], 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate );
    calcHist( &RGB[1], 1, 0, Mat(), g_hist, 1, &histSize, &histRange, uniform, accumulate );
    calcHist( &RGB[0], 1, 0, Mat(), r_hist, 1, &histSize, &histRange, uniform, accumulate );

    // Draw the histograms for B, G and R
    int hist_w = 512; int hist_h = 400;
    int bin_w = cvRound( (double) hist_w/histSize );

    Mat histImage( hist_h, hist_w, CV_8UC3, Scalar( 0,0,0) );

    /// Normalize the result to [ 0, histImage.rows ]
    normalize(b_hist, b_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
    normalize(g_hist, g_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
    normalize(r_hist, r_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );

    /// Draw for each channel
    for( int i = 1; i < histSize; i++ ) {
        line( histImage, Point( bin_w*(i-1), hist_h - cvRound(b_hist.at<float>(i-1)) ) ,
              Point( bin_w*(i), hist_h - cvRound(b_hist.at<float>(i)) ),
              Scalar( 255, 0, 0), 2, 8, 0  );
        line( histImage, Point( bin_w*(i-1), hist_h - cvRound(g_hist.at<float>(i-1)) ) ,
              Point( bin_w*(i), hist_h - cvRound(g_hist.at<float>(i)) ),
              Scalar( 0, 255, 0), 2, 8, 0  );
        line( histImage, Point( bin_w*(i-1), hist_h - cvRound(r_hist.at<float>(i-1)) ) ,
              Point( bin_w*(i), hist_h - cvRound(r_hist.at<float>(i)) ),
              Scalar( 0, 0, 255), 2, 8, 0  );
    }

    /// Display
    QLabel *histogram = new QLabel;
    histogram->setPixmap(QPixmap::fromImage(cvMatToQImage(histImage)));
    histogram->show();
}
