#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>

double pi = M_PI;

using namespace cv;

MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->graphicsView->setScene(&scene);
}

MainWindow::~MainWindow()
{
    delete ui;
}

Mat binarizeImage(Mat &image, int thresholdValue)
{
    Mat bin = Mat();
    threshold(image, bin, thresholdValue, 255, THRESH_BINARY);
    imwrite("binarized.bmp", bin);
    return bin;
}

double square(double x) {return x*x;}

void MainWindow::on_movementPushButton_clicked()
{
    QStringList files = QFileDialog::getOpenFileNames(this);
    Mat *matrixesRgb = new Mat[files.size()];
    Mat *matrixesGray = new Mat[files.size()];

    for(int i=0; i<files.size(); i++) {
        matrixesRgb[i] = imread(files[i].toStdString());
        if (matrixesRgb[i].data == NULL){
            QMessageBox::critical(this, "Image Load", "Cannot load image: "+files[i]);
            return;
        }
        cvtColor(matrixesRgb[i], matrixesGray[i], CV_RGB2GRAY);
    }

    Mat bin = binarizeImage(matrixesGray[0], 70);

    std::vector<Point2f> corners;
    goodFeaturesToTrack(matrixesGray[0], corners, 500, 0.01, 50, bin);

    QVector< QVector<Point2f> > allPoints(corners.size());
    for(int i=0; i<allPoints.size(); i++)
        allPoints[i].push_back(corners[i]);

    for(unsigned int i=0; i<corners.size(); i++)
        ellipse(matrixesRgb[0], corners[i], Size(2,2), 0, 0, 360, Scalar(0, 255, 0), 2);

    //    updateImage(matrixesRgb[0], ui->graphicsView_2);

    for(int i=1; i<files.size(); i++) {
        std::vector<Point2f> cornersOld = corners;
        corners.clear();
        std::vector<uchar> featuresFound;
        Mat err;
        calcOpticalFlowPyrLK(matrixesGray[i-1], matrixesGray[i], cornersOld, corners, featuresFound, err);

        for(uint j=0; j<corners.size(); j++) {
            double n = norm(corners[j]-cornersOld[j]);
            qDebug() << n;
            if(n > 100)
                corners.erase(corners.begin()+j);
        }

        for(unsigned int j=0; j<corners.size(); j++)
            ellipse(matrixesRgb[i], corners[j], Size(2,2), 0, 0, 360, Scalar(0, 255, 0), 2);

        int tmp = 0;
        for(int j=0; j<allPoints.size(); j++)
            if(featuresFound[j])
                allPoints[j].push_back(corners[tmp++]);
            else
                qDebug() << "Feature for "+QString::number(i)+" not found";
        qDebug() << "4";
    }

    for(int i=0; i<allPoints.size(); i++) {
        if(allPoints[i].size() < 2)
            continue;

        for(int j=0; j<allPoints[i].size()-1; j++)
            for(int k=0; k<files.size(); k++){
                Point2f start = allPoints[i][j],
                        end   = allPoints[i][j+1];
                Point2f newend ;
                newend.x=start.x-fabs(end.x-start.x)*2;
                newend.y=start.y-fabs(end.y-start.y)*2;
                line(matrixesRgb[k], start, newend, Scalar(0, 255, 255), 1);
                 ellipse(matrixesRgb[k], newend, Size(2,2), 0, 0, 360, Scalar(192,64, 0), 2);
                Scalar line_color =Scalar(0, 0, 255);
                line(matrixesRgb[k], start, end, line_color, 1);


            }
    }

    for(int i=0; i<files.size(); i++)
        imwrite((QString::number(i+1)+".bmp").toStdString(), matrixesRgb[i]);
}

void MainWindow::on_saveMovementPushButton_clicked()
{

}
