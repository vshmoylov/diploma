#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QGraphicsScene>

namespace Ui {
class MainWindow;
}

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_movementPushButton_clicked();

    void on_saveMovementPushButton_clicked();

private:
    Ui::MainWindow *ui;
    QGraphicsScene scene;

};

#endif // MAINWINDOW_H
