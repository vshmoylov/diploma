#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QAtomicInt>
#include <QGraphicsScene>
#include <opencv2/core/core.hpp>

#define DEBUG_VERSION

namespace Ui {
class Widget;
}
class QGraphicsScene;
namespace cv {
class Mat;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_pushButton_clicked();

    void on_tabWidget_currentChanged(int index);

    void on_fileListWidget_itemSelectionChanged();

    void on_pushButton_2_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_4_clicked();

    void updateImage();

    void on_comboBox_currentIndexChanged(int index);

    void generatePreview();

    void on_executePushButton_clicked();

    void concurrentProcessImages();

    void disableControls();
    void enableControls();
    void setProgress(int value);


protected:
    void resizeEvent(QResizeEvent *evt);

private:
    enum PreviewMode {OriginalImage = 0, ChannelH, ChannelS, ChannelV, Mask, ImageAndMask, ResultImage };
    void updateProgressCount();
    void clearImages();
    void processImage(const cv::Mat &img);
    void saveResult(QString dir, QString filename);

#ifdef DEBUG_VERSION
    void preloadImg();
#endif
private:
    Ui::Widget *ui;
    QGraphicsScene scene;
    cv::Mat originalImg;
    QVector<cv::Mat> HSV;
    cv::Mat mask;
    cv::Mat resultImg;
    QAtomicInt toStop;
    bool running;
};

#endif // WIDGET_H
