#include "widget.h"
#include "ui_widget.h"
#include "ImageTransformations.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QtGui>
#include <QtConcurrent/QtConcurrent>
#include <QDebug>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


using namespace cv;

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    ui->graphicsView->setScene(&scene);

    ui->tabWidget->setTabEnabled(1, false);

    connect(ui->pushButton_3, SIGNAL(clicked()), ui->fileListWidget, SLOT(clear()));

    connect(ui->hMinSpinBox, SIGNAL(valueChanged(int)), ui->hMinHorizontalSlider, SLOT(setValue(int)));
    connect(ui->hMinHorizontalSlider, SIGNAL(valueChanged(int)), ui->hMinSpinBox, SLOT(setValue(int)));
    connect(ui->hMaxSpinBox, SIGNAL(valueChanged(int)), ui->hMaxHorizontalSlider, SLOT(setValue(int)));
    connect(ui->hMaxHorizontalSlider, SIGNAL(valueChanged(int)), ui->hMaxSpinBox, SLOT(setValue(int)));

    connect(ui->sMinSpinBox, SIGNAL(valueChanged(int)), ui->sMinHorizontalSlider, SLOT(setValue(int)));
    connect(ui->sMinHorizontalSlider, SIGNAL(valueChanged(int)), ui->sMinSpinBox, SLOT(setValue(int)));
    connect(ui->sMaxSpinBox, SIGNAL(valueChanged(int)), ui->sMaxHorizontalSlider, SLOT(setValue(int)));
    connect(ui->sMaxHorizontalSlider, SIGNAL(valueChanged(int)), ui->sMaxSpinBox, SLOT(setValue(int)));

    connect(ui->vMinSpinBox, SIGNAL(valueChanged(int)), ui->vMinHorizontalSlider, SLOT(setValue(int)));
    connect(ui->vMinHorizontalSlider, SIGNAL(valueChanged(int)), ui->vMinSpinBox, SLOT(setValue(int)));
    connect(ui->vMaxSpinBox, SIGNAL(valueChanged(int)), ui->vMaxHorizontalSlider, SLOT(setValue(int)));
    connect(ui->vMaxHorizontalSlider, SIGNAL(valueChanged(int)), ui->vMaxSpinBox, SLOT(setValue(int)));

    ui->hMinSpinBox->setValue(0);
    ui->hMaxSpinBox->setValue(180);
    ui->sMinSpinBox->setValue(0);
    ui->sMaxSpinBox->setValue(80);
    ui->vMinSpinBox->setValue(35);
    ui->vMaxSpinBox->setValue(256);

    connect (ui->cropDoubleSpinBox,          SIGNAL(valueChanged(double)),  this, SLOT(generatePreview()));
    connect (ui->cropGroupBox,               SIGNAL(toggled(bool)),         this, SLOT(generatePreview()));
    connect (ui->normalizeHistogramCheckBox, SIGNAL(toggled(bool)),         this, SLOT(generatePreview()));
    connect (ui->quantizeGroupBox,           SIGNAL(toggled(bool)),         this, SLOT(generatePreview()));
    connect (ui->quantizeSpinBox,            SIGNAL(valueChanged(int)),     this, SLOT(generatePreview()));
    connect (ui->hMinSpinBox,                SIGNAL(valueChanged(int)),     this, SLOT(generatePreview()));
    connect (ui->hMaxSpinBox,                SIGNAL(valueChanged(int)),     this, SLOT(generatePreview()));
    connect (ui->sMinSpinBox,                SIGNAL(valueChanged(int)),     this, SLOT(generatePreview()));
    connect (ui->sMaxSpinBox,                SIGNAL(valueChanged(int)),     this, SLOT(generatePreview()));
    connect (ui->vMinSpinBox,                SIGNAL(valueChanged(int)),     this, SLOT(generatePreview()));
    connect (ui->vMaxSpinBox,                SIGNAL(valueChanged(int)),     this, SLOT(generatePreview()));


    running = false;

#ifdef DEBUG_VERSION
    ui->outputDirLineEdit->setText("C:\\temp\\result");
    preloadImg();
#endif

}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_clicked()
{
    QStringList fileList = QFileDialog::getOpenFileNames(this,
                                                         QString::fromUtf8("Добавить изображения"),
                                                         QString(),
                                                         QString::fromUtf8("Изображения (*.png *.bmp *.jpg);;Список файлов (*.txt);;Все файлы (*.*)"));
    foreach (const QString &fileName, fileList) {
        if (fileName.endsWith(".txt")){
            QFile file(fileName);
            if (file.open(QIODevice::ReadOnly|QIODevice::Text)){
                QStringList list = QString(file.readAll()).split('\n');
                foreach (const QString &line, list) {
                    if (QFile::exists(line))
                        ui->fileListWidget->addItem(line);
                }
            }
        } else {
            ui->fileListWidget->addItem(fileName);
        }
    }
    updateProgressCount();
}

void Widget::on_tabWidget_currentChanged(int index)
{
    if (index==1){
        generatePreview();
        updateImage();
    }
}

void Widget::on_fileListWidget_itemSelectionChanged()
{
    if (ui->fileListWidget->currentRow()!=-1)
        ui->tabWidget->setTabEnabled(1, true);
    else ui->tabWidget->setTabEnabled(1, false);
}

void Widget::on_pushButton_2_clicked()
{
    QString dirName = QFileDialog::getExistingDirectory(this, QString::fromUtf8("Добавить директорию"));
    if (dirName.isEmpty()) return;
    QDir dir(dirName);
    QStringList fileList = dir.entryList(QStringList() << "*.jpg" << "*.bmp" << "*.png");
    QStringList list;
    foreach (const QString &fileName, fileList) {
        list << dir.absoluteFilePath(fileName);
    }

    ui->fileListWidget->addItems(list);
    updateProgressCount();
}

void Widget::on_pushButton_6_clicked()
{
    delete ui->fileListWidget->currentItem();
}

void Widget::updateProgressCount()
{
    ui->progressBar->setMaximum(ui->fileListWidget->count());
}

void Widget::clearImages()
{
    originalImg = Mat();
    HSV = QVector<Mat>(3);
    mask = Mat();
    resultImg = Mat();
}

void Widget::processImage(const Mat &img)
{
    clearImages();
    originalImg = img;
    QVector<Mat> bgr(3);

    split(originalImg,bgr.data());
    Mat hsvMat;
    cvtColor(originalImg, hsvMat, CV_BGR2HSV);
    split(hsvMat, HSV.data());

    int Hmin = ui->hMinSpinBox->value();
    int Hmax = ui->hMaxSpinBox->value();
    int Smin = ui->sMinSpinBox->value();
    int Smax = ui->sMaxSpinBox->value();
    int Vmin = ui->vMinSpinBox->value();
    int Vmax = ui->vMaxSpinBox->value();
    Mat h_range, s_range, v_range;
    inRange(HSV[0], Scalar(Hmin), Scalar(Hmax), h_range);
    inRange(HSV[1], Scalar(Smin), Scalar(Smax), s_range);
    inRange(HSV[2], Scalar(Vmin), Scalar(Vmax), v_range);
    bitwise_and(h_range, s_range, mask);
    bitwise_and(mask, v_range, mask);

    resultImg = copyMask(bgr[2], mask);

    if (ui->cropGroupBox->isChecked()){
        int x = originalImg.cols/2;
        int y = originalImg.rows/2;
        int r = (originalImg.cols*ui->cropDoubleSpinBox->value()/100.0)/2;
        int newSize = r*2;
        int cropX = (originalImg.cols-newSize)/2;
        int cropY = 0;
        int cropWidth = newSize;
        int cropHeight = originalImg.rows;
        resultImg = /*letterboxImage(*/cropImage(cutCircle(resultImg, x, y, r),cropX, cropY, cropWidth, cropHeight)/*, newSize, newSize)*/;
    }
    if (ui->normalizeHistogramCheckBox->isChecked()){
        Mat temp;
        equalizeHist(resultImg, temp);
        resultImg = temp;
    }
    if (ui->quantizeGroupBox->isChecked()){
        resultImg = quantize(resultImg,ui->quantizeSpinBox->value());
    }
}

void Widget::saveResult(QString dir, QString filename)
{

    QString dirName = QDir::fromNativeSeparators(dir);
    QDir path;
    path.mkpath(dirName);

    if (!dirName.endsWith('/')) dirName += '/';
    QFileInfo fi(filename);
    QString outputFileName = dirName + fi.fileName();
    imwrite(outputFileName.toStdString(), resultImg);
}

void Widget::setProgress(int value)
{
    ui->progressBar->setValue(value);
}

void Widget::generatePreview()
{
    clearImages();
    QString fileName = ui->fileListWidget->currentItem()->text();
    Mat img = imread(fileName.toStdString());
    Mat temp;
    cv::resize(img,temp,Size(1000, img.rows*(1000.0/img.cols)));
    processImage(temp);


    updateImage();
}


#ifdef DEBUG_VERSION
void Widget::preloadImg()
{
    QFile imagesList("../test1/ImagesList.txt");
    if (!imagesList.open(QIODevice::ReadOnly)) return;
    QString fname = imagesList.readLine().trimmed();
    imagesList.close();
    ui->fileListWidget->addItem(fname);
}
#endif

void Widget::on_pushButton_4_clicked()
{
    QString outputDir = QFileDialog::getExistingDirectory(this, QString::fromUtf8("Выберите папку для сохранения"));
    if (!outputDir.isEmpty())
        ui->outputDirLineEdit->setText(outputDir);
}

void Widget::updateImage()
{
    QImage img;
    switch (ui->comboBox->currentIndex()) {
    case OriginalImage:
        img = cvMatToQImage(originalImg);
        break;
    case ChannelH:
        img = cvMatToQImage(HSV[0]);
        break;
    case ChannelS:
        img = cvMatToQImage(HSV[1]);
        break;
    case ChannelV:
        img = cvMatToQImage(HSV[2]);
        break;
    case Mask:
        img = cvMatToQImage(mask);
        break;
    case ImageAndMask:
        img = cvMatToQImage(blendImages(originalImg, grayToBGR(mask)));
        break;
    case ResultImage:
        img = cvMatToQImage(resultImg);
        break;
    }

    scene.clear();
    scene.setSceneRect(ui->graphicsView->rect());
    scene.addPixmap(QPixmap::fromImage(img).scaled(ui->graphicsView->size().width()-3,
                                                   ui->graphicsView->size().height()-3,
                                                   Qt::KeepAspectRatio,Qt::SmoothTransformation));
}

void Widget::resizeEvent(QResizeEvent *evt)
{
    updateImage();
    evt->accept();
}

void Widget::on_comboBox_currentIndexChanged(int index)
{
    updateImage();
}

void Widget::on_executePushButton_clicked()
{
    if (running){
        ui->executePushButton->setEnabled(false);
        ui->executePushButton->setText(QString::fromUtf8("Запуск"));
        toStop.store(1);
        running = false;
    } else {
        ui->executePushButton->setText(QString::fromUtf8("Стоп"));
        disableControls();

        updateProgressCount();
        toStop.store(0);
        QtConcurrent::run(this, &Widget::concurrentProcessImages);
        running = true;
    }

}

void Widget::concurrentProcessImages()
{
    QString outputDir = ui->outputDirLineEdit->text();
    for (int i=0; i<ui->fileListWidget->count(); i++){
        if (toStop.load()!=0){
            QMetaObject::invokeMethod(this, "enableControls");
            return;
        }
        ui->fileListWidget->setCurrentRow(i);
        QString fileName = ui->fileListWidget->item(i)->text();
        qDebug() << "Current image number: " <<i << "; name: " << fileName;

        Mat image = imread(fileName.toStdString());
        processImage(image);
        saveResult(outputDir, fileName);
//        ui->progressBar->setValue(i+1);
        QMetaObject::invokeMethod(this, "setProgress", Qt::QueuedConnection, Q_ARG(int, i+1));
//        setProgress(i+1);
    }
    QMetaObject::invokeMethod(this, "enableControls");
}



void Widget::disableControls()
{
    ui->controlsWidget->setEnabled(false);
    ui->outputDirLineEdit->setEnabled(false);
    ui->pushButton_4->setEnabled(false);
    ui->fileListWidget->setEnabled(false);
    ui->tabWidget->setTabEnabled(1, false);
    QApplication::setOverrideCursor(Qt::WaitCursor);
}

void Widget::enableControls()
{
    ui->controlsWidget->setEnabled(true);
    ui->outputDirLineEdit->setEnabled(true);
    ui->pushButton_4->setEnabled(true);
    ui->fileListWidget->setEnabled(true);
    ui->tabWidget->setTabEnabled(1, true);
    toStop.store(0);
    QApplication::restoreOverrideCursor();
    ui->executePushButton->setEnabled(true);
}
